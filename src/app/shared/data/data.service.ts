import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public refreshList = new Subject<boolean>();


  public sideNavData = new BehaviorSubject<any>(1);

  constructor(
  ) {}
}
