import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListRootComponent } from './list-root.component';
import { TabComponent } from './containers/tab/tab.component';
import { ListRoutingModule } from './listing-route';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ListRootComponent, TabComponent],
  imports: [
    CommonModule,
    ListRoutingModule,
    SharedModule
  ]
})
export class ListingModule { }
