import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListRootComponent } from './list-root.component';
import { TabComponent } from './containers/tab/tab.component';

const routes: Routes = [
  {
    path: '',
    component: ListRootComponent,
    children: [
      {
        path: '',
        component: TabComponent,
      },
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class ListRoutingModule { }
