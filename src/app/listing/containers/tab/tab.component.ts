import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ListConstant } from '../../constants/list-constant';
import { DataService } from 'src/app/shared/data/data.service';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {
  eventId;
  doNotInclude;
  tabs = [];
  events = ListConstant.events;
  selected = new FormControl(0);
  constructor( public route: ActivatedRoute, private dataService: DataService
    ) {
    const params = this.route.params;
    this.eventId = params['value'].id;
  }
  ngOnInit() {
    this.dataService.sideNavData.subscribe(data => {
      if (data) {
        this.doNotInclude = false;
        this.tabs.forEach(tab => {
          if (Number(tab.id) === Number(data)) {
            this.doNotInclude = true;
          }
        });
        if (!this.doNotInclude) {
          this.tabs.push(this.events[Number(data) - 1]);
        }
      }
    });
    if (this.eventId) {
      this.tabs.push(this.events[Number(this.eventId) - 1]);
    }
  }

  removeTab(index) {
    this.tabs.splice(index, 1);
  }
}
