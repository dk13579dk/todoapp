import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/shared/data/data.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  navLinks: [
    {link: 1, name: 'Tab 1'},
    {link: 2, name: 'Tab 2'},
    {link: 3, name: 'Tab 3'},
    {link: 4, name: 'Tab 4'},
    {link: 5, name: 'Tab 5'},
    {link: 6, name: 'Tab 6'},
    {link: 7, name: 'Tab 7'},
    {link: 8, name: 'Tab 8'},
  ];
  afterSelect: EventEmitter<any> = new EventEmitter<any>();
  constructor(private router: Router, private dataService: DataService) { }

  ngOnInit() {
  }
  changeRoute(navId) {
    this.dataService.sideNavData.next(navId);
  }
  changeRoute1(nav){
    this.router.navigate(['list', nav.link]);
  }
}
