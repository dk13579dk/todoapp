import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../container/home/home.component';
import { PageNotFoundComponent } from '../container/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: 'list',
    loadChildren:
      '../../listing/listing.module#ListingModule',
  },
  {
    path: '',
    component: HomeComponent
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
