import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideBarComponent } from './container/side-bar/side-bar.component';
import { SharedModule } from '../shared/shared.module';
import { AppRoutingModule } from './modules/app-routing.module';
import { HomeComponent } from './container/home/home.component';
import { PageNotFoundComponent } from './container/page-not-found/page-not-found.component';

@NgModule({
  declarations: [SideBarComponent, HomeComponent, PageNotFoundComponent],
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule
  ],
  exports: [
    SideBarComponent,
    AppRoutingModule
  ]
})
export class CoreModule { }
