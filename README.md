# EventApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.
Download or clone it from repository and type `npm install`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build:qa` and `npm run build:prod` to build the project for Qa /Prod. The build artifacts will be stored in the `www/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Useful Angular cli Instructions

 ng g component   // by default to app module  
 
 // Custom Module Generation and adding components/services to it
 
 ng g module `CustomModule` --routing
 ng g component `Component Name` -m `CustomModule`
 ng g service `Service Name` -m `CustomModule`

## Build Directory

(www) => {
// Added www as output path to build scripts in package.json 
// Because by default build command creates dist folder which keeps on deleting on ng serve.
// So, to avoid that mess of dist folder we added `www` as build folder 
}

## Directory Structure

source link: https://bulldogjob.com/articles/539-scalable-angular-application-architecture

AppModule – the bootstrapping module, responsible for launching the application and combining other modules together
Core –  core functionalities, that will be used in the whole application globally. They should not be imported by other application modules.
Shared – usually a set of components or services that will be reused in other application modules, not applied globally. 
         They can be imported by feature modules.

All remaining modules (so-called feature modules) are isolated and independent.

## Core

(src --> app --> core) => {
// it's a core module. it's App's Core entities which are included in app.module.ts and would not be imported by feature modules. 
// These are global containers, modules and constants. 

const `App's Core Includes` = [  
`App Containers like Home, SideNav and PageNotFound`,
`App Modules like app routing module and app material module`
];
};

## Shared

(src --> app --> shared) => {
// It's a shared module. It's App's shared entities which are included in app.module.ts. 
// And would be shared by all feature modules including App module. 

 const `App's Shared Includes` = [
`App Shared Services like  Data as of now`,
`Will Include App Shared Components` 
 ];
};

## Modules

(src --> app --> modules (user, procurement, marketing, catalog...etc) ) => {
// These are the independent feature modules which are configured in app.routing.module.ts

const `Module Includes` = [
`Constants`,
`Containers`,
`Module Root Component for including containers via router outlet.`
];
};
